package com.company;

import java.util.Scanner;

public class Main {

    public static void printArray(int[][] array) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(array[i][j] + "  ");
            }
            System.out.println();
            System.out.println();
        }
    }

    public static boolean isThereWinner(int[][] array) {
        if (array[0][0] == array[0][1] & array[0][0] == array[0][2] & array[0][0] != 2) {
            return true;
        }
        if (array[1][0] == array[1][1] & array[1][2] == array[1][0] & array[1][0] != 2) {
            return true;
        }
        if (array[2][0] == array[2][1] & array[2][2] == array[2][0] & array[2][0] != 2) {
            return true;
        }

        if (array[0][0] == array[1][0] & array[0][0] == array[2][0] & array[0][0] != 2) {
            return true;
        }
        if (array[0][1] == array[1][1] & array[0][1] == array[2][1] & array[0][1] != 2) {
            return true;
        }
        if (array[0][2] == array[1][2] & array[0][2] == array[2][2] & array[0][2] != 2) {
            return true;
        }

        if (array[0][0] == array[1][1] & array[0][0] == array[2][2] & array[0][0] != 2) {
            return true;
        }
        if (array[2][0] == array[1][1] & array[2][0] == array[0][2] & array[2][0] != 2) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] tickTackArray = {{2, 2, 2}, {2, 2, 2}, {2, 2, 2}};
        System.out.println("Instructions:");
        System.out.println("1 = X, 0 = O");
        System.out.println("Write line and column, separate them with space");
        System.out.println("First move makes \"X\"");
        System.out.println("Fight!!!");
        for (int i = 0; i < 9; i++) {
            printArray(tickTackArray);
            if (i % 2 == 0) {
                System.out.println("Player 1 (X):");
            } else {
                System.out.println("Player 2 (O):");
            }

            int line = scanner.nextInt();
            int column = scanner.nextInt();
            if (line > 2 || column > 2 || tickTackArray[line][column] == 1 || tickTackArray[line][column] == 0) {
                while (line < 0 || line > 2 || column < 0 || column > 2 || tickTackArray[line][column] != 2) {
                    System.out.println("ERROR, write correct coordinates");
                    line = scanner.nextInt();
                    column = scanner.nextInt();
                }
            }
            if (i % 2 == 0) {
                tickTackArray[line][column] = 1;
            } else {
                tickTackArray[line][column] = 0;
            }

            if (isThereWinner(tickTackArray)) {
                if (i % 2 == 0) {
                    System.out.println("Player 1 WINS");
                } else {
                    System.out.println("Player 2 WINS");
                }
                return;
            }

        }
        System.out.println("DRAW!");

    }
}
